import React from 'react';

class App extends React.Component {
    render() {
        return (
            <div>
                <h2>React Portlet (c)Jani Nieminen, 2017</h2>
            </div>
        );
    }

}

export default App;
