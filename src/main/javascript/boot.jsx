import React from 'react';
import { render } from 'react-dom';

import { BackendLiferay } from './services/backend_liferay';

import App from './components/app.jsx';

function start(appRootElement, portletConfig) {

    let backend = null;
        backend = new BackendLiferay(portletConfig);

    const context = {
        backend
    };

    render(<App portletConfig={portletConfig} context={context}/>, appRootElement);
}

global.startReactPortletDemoApp = start;
